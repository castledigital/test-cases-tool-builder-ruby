# -*- encoding: utf-8 -*-
$LOAD_PATH.unshift File.expand_path("../lib", __FILE__)
require "test-cases-tool-builder-ruby/version"

Gem::Specification.new do |s|
  s.name          = 'test-cases-tool-builder-ruby'
  s.version       = TestCasesToolBuilder::Version::STRING
  s.platform      = Gem::Platform::RUBY
  s.authors       = ['Ilya Zhilenkov']
  s.email         = ['']
  s.description   = %q{No description}
  s.summary       = "test-cases-tool-builder-ruby-#{TestCasesToolBuilder::Version::STRING}"
  s.homepage      = ''
  s.license       = 'Apache-2.0'

  s.files         = `git ls-files`.split("\n")
  s.test_files    = `git ls-files -- {spec,features}/*`.split("\n")
  s.executables   = `git ls-files -- bin/*`.split("\n").map{ |f| File.basename(f) }
  s.require_paths = ['lib']

  s.add_dependency 'mimemagic'

  s.add_development_dependency 'bundler'
  s.add_development_dependency 'rake'
end
