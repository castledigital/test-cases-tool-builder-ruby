require 'test-cases-tool-builder-ruby/builder'

module TestCasesToolBuilder
  module Config
    class << self
      attr_accessor :api_key
      attr_accessor :url
      attr_accessor :local_path
      attr_accessor :types

      def api_key
        ENV["TEST_CASES_TOOL_API_KEY"] || @api_key
      end

      def url
        @url || 'http://localhost:3000'
      end

      def local_path
        @local_path || "."
      end

      def types
        if @types
          @types.class == Array ? @types.map(&:to_sym) : [@types.to_sym]
        else
          []
        end
      end
    end
  end
  class << self
    def configure(&block)
      yield Config
    end
  end
end
