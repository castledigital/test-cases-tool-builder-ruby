module TestCasesToolBuilder
  class Builder
    class << self
      attr_accessor :scenario
      attr_accessor :suite
      attr_accessor :steps
      attr_accessor :precondition
      attr_accessor :can_send

      def start_suite
        @suite ||={}
        @suite = {
            branch: `git rev-parse --abbrev-ref HEAD`.strip,
            commit: `git log -1 --pretty=format:"%H"`.strip,
            time: Time.now,
            api_key: "#{TestCasesToolBuilder::Config.api_key}"
        }
        self.precondition ||= []
        @can_send = false
      end

      def start_scenario
        @precondition ||= []
        @scenario ||=[]
        @steps = []
      end

      def init_group_started
        @precondition = []
        @can_send = true
      end

      def add_precondition(precondition)
        self.precondition ||= []
        self.precondition.push(precondition)
      end

      def build_scenario(scenario_info={})
        unless scenario_info.empty?
          prepare_steps(scenario_info[:code_block])
          @steps.empty? ? steps = [] : steps = @steps
          @scenario.push({
                             directory: scenario_info[:directory],
                             feature: scenario_info[:feature],
                             scenario: scenario_info[:scenario],
                             steps: steps,
                             preconditions: @precondition,
                             duration: scenario_info[:duration].to_s,
                             status: scenario_info[:status],
                             location: scenario_info[:location],
                             exception: scenario_info[:exception]
                         })
        else
          raise "Scenario info is empty"
        end
      end

      def step(step, status)
        status = :failed if status.nil?
        @steps.push({step: step, status: status})
      end

      def stop_suite(status, duration)
        @suite.merge!({
                          status: status,
                          duration: duration,
                          scenarios: @scenario
                      })
        send_data! if @can_send
      end

      private

      def send_data!
        if ENV["TEST_CASES_TOOL_LOCAL_PATH"]
          File.open(ENV["TEST_CASES_TOOL_LOCAL_PATH"], 'w') { |file| file.write(@suite.to_json) }
        else
          uri = URI.parse("#{TestCasesToolBuilder::Config.url}/api/parse_report")
          http = Net::HTTP.new(uri.host, uri.port)
          http.use_ssl = true
          headers = {
              'Content-Type'=> 'application/json'
          }
          begin
            res = http.post(uri.path, @suite.to_json, headers)
          rescue => e
            file = "#{TestCasesToolBuilder::Config.local_path}/local_report_#{Time.now.to_i}.json"
            File.open(file, 'w') { |file| file.write(@suite.to_json) }
            raise "\e[#{31}mThere is something bad happened... so I saved result json here #{file}\e[0m\n"
            raise "#{e}"
          end
          if res.code == 200
            hash = JSON.parse(res.body, symbolize_names: true)
            if hash[:status] == "ok"
              puts "\e[#{32}m#{hash[:message]}\e[0m"
            else
              file = "#{TestCasesToolBuilder::Config.local_path}/local_report_#{Time.now.to_i}.json"
              File.open(file, 'w') { |file| file.write(@suite.to_json) }
              puts "\e[#{31}mSomething going wrong on upload report :(\n#{hash[:message]}\nFile saved here #{file}\e[0m"
            end
          else
            file = "#{TestCasesToolBuilder::Config.local_path}/local_report_#{Time.now.to_i}.json"
            File.open(file, 'w') { |file| file.write(@suite.to_json) }
            puts "\e[#{31}mSomething going wrong on upload report :(\nresponce code is #{res.code}\nFile saved here #{file}\e[0m"
          end
        end
      end

      def prepare_steps(scenario_as_string)
        whole_steps = []
        run_steps = []
        scenario_as_string.each_line do |line|
          if line.match(/step (.*) do/)
            whole_steps.push(line.scan(/step (.*) do/).last.last.chomp('"').reverse.chomp('"').reverse.chomp("'").reverse.chomp("'").reverse)
          elsif line.match(/step (.*)/)
            whole_steps.push(line.scan(/step (.*)/).last.last.chomp('"').reverse.chomp('"').reverse.chomp("'").reverse.chomp("'").reverse)
          end
        end
        @steps.each do |s|
          run_steps.push(s[:step])
        end
        (whole_steps - run_steps).each do |p|
          @steps.push({step: p, status: :pending})
        end
      end
    end
  end
end
